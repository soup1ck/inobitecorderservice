package com.inobitec.orderservice.model;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class OrderItem {

    private Integer id;
    private Integer orderId;
    private String itemName;
}