package com.inobitec.orderservice.service;

import com.inobitec.orderservice.repository.OrderItemRepository;
import com.inobitec.orderservice.repository.OrderRepository;
import com.inobitec.orderservice.model.Order;
import com.inobitec.orderservice.model.OrderItem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;

    public void createOrder(Order order) {
        order.setId(orderRepository.getNextOrderId());
        orderRepository.insertOrder(order);
        for (OrderItem orderItem : order.getOrderItems()) {
            orderItem.setId(orderItemRepository.getNextOrderItemId());
            orderItem.setOrderId(order.getId());
            orderItemRepository.insertOrderItem(orderItem);
        }
    }

    public Order getOrderById(Integer id) {
        return orderRepository.getOrderById(id);
    }

    public void updateOrderById(Order order) {
        Order orderFromDb = orderRepository.getOrderById(order.getId());
        if (orderFromDb.getOrderItems().size() == order.getOrderItems().size()) {
            orderRepository.updateOrder(order);
            for (OrderItem orderItem : order.getOrderItems()) {
                orderItemRepository.updateOrderItem(orderItem);
            }
        }
    }

    public  void deleteOrder(Integer id){
        orderItemRepository.deleteOrderItem(id);
        orderRepository.deleteOrder(id);
        }
    }


//        } else {
//            for (OrderItem orderItemFromDb:orderFromDb.getOrderItems()) {
//                Integer idItemFromDb=orderItemFromDb.getId();
//                for(OrderItem orderItem : order.getOrderItems()){
//                    if(!idItemFromDb.equals(orderItem.getId())){
//                        orderItemMapper.insertOrderItem(orderItem);
//                    }
//                }
//            }
//        }
//            for(int i =0;i<order.getOrderItems().size();i++){
//                if(!order.getOrderItems().get(i).getId().equals(orderFromDb.getOrderItems().get(i).getId())){
//                    orderItemMapper.insertOrderItem(order.getOrderItems().get(i));
//            Integer orderItemId = order.getOrderItems().get(i).getOrderId();
//            orderMapper.getOrderById(id).getOrderItems().size();