package com.inobitec.orderservice.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

public class SwaggerConfiguration {

    public OpenAPI orderServiceApi() {
        return new OpenAPI()
                .info(
                        new Info().title("Test Order Microservice")
                                .version("1.0.0")
                                .description("Order microservice REST API v1.0")
                );
    }
}

