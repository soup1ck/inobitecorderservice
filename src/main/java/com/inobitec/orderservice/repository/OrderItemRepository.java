package com.inobitec.orderservice.repository;

import com.inobitec.orderservice.model.OrderItem;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface OrderItemRepository {

    @Select("SELECT * FROM order_item WHERE order_id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "orderId", column = "order_id"),
            @Result(property = "itemName", column = "item_name")
    })
    List<OrderItem> selectOrderItems(Integer id);

    @Select("SELECT nextval('order_item_seq') ")
    Integer getNextOrderItemId();

    @Insert("INSERT into order_item (id, order_id, item_name) " +
            "VALUES (#{id}, #{orderId}, #{itemName})")
    void insertOrderItem(OrderItem orderItem);


    @Update("UPDATE order_item SET item_name = #{order_item.itemName} where id=#{order_item.id}")
    void updateOrderItem(@Param("order_item") OrderItem orderItem);

    @Delete("DELETE  from order_item where order_id=#{id} ")
    void deleteOrderItem(Integer id);
}
