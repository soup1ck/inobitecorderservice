package com.inobitec.orderservice.repository;

import com.inobitec.orderservice.model.Order;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface OrderRepository {

    @Insert("INSERT into \"order\" (id, order_status_id, customer_name, customer_phone,customer_comment) " +
            "VALUES (#{id}, #{orderStatusId}, #{customerName}, #{customerPhone}, #{customerComment})" )
    void insertOrder(Order order);

    @Select("SELECT * from \"order\" where id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id",id = true),
            @Result(property = "orderStatusId", column = "order_status_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerPhone", column = "customer_phone"),
            @Result(property = "customerComment", column = "customer_comment"),
            @Result(property = "orderItems", column = "id", javaType = List.class,
                    many = @Many(select = "com.inobitec.orderservice.repository.OrderItemRepository.selectOrderItems"))
    })
    Order getOrderById(Integer id);

    @Select("SELECT nextval('order_seq') ")
    Integer getNextOrderId();

    @Update("UPDATE \"order\" SET order_status_id=#{order.orderStatusId}, " +
            "customer_name=#{order.customerName}," +
            " customer_phone=#{order.customerPhone}," +
            " customer_comment=#{order.customerComment} " +
            "WHERE id=#{order.id}")
    void updateOrder(@Param("order") Order order);

    @Delete("DELETE  from \"order\" where id=#{id} ")
    void deleteOrder(Integer id);
}
